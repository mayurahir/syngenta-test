// Import Request module for making API requests
require('request');
const Request = require('request-promise-native');

// Import & configure Express web server
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const port = process.env.PORT || 3000;

// Express enable CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

// parse application/x-www-form-urlencoded data with the 'qs' library
app.use(bodyParser.urlencoded({extended: true}));

// Express parse application/json
app.use(bodyParser.json());

app.listen(port, () => console.log('Node app is ruuning on PORT:', port));

// Constants
const API_BASE_PATH = '0-0-5.inyourarea.co.uk';
const API_URL = `https://${API_BASE_PATH}/facade/city`;

// Default options for API Requests
let defaultOptions = {
    uri: '',
    headers: {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache'
    },
    json: true
};

// Returns Promise of fetching API Data
let fetchAPIData = (url) => {
    let options = Object.assign({}, defaultOptions);
    options.uri = url;

    console.log('fetchAPIData', 'Sending request to external API', options);

    return new Promise((resolve, reject) => {
        Request.get(options)
            .then((data) => {
                resolve(data);
            })
            .catch((err) => {
                if (err.error) {
                    console.error('GET', url, 'Problem with request:', err.error);
                    reject(err.error);
                } else {
                    console.error('GET', url, 'Problem with request:', 'API request failed.');
                    reject({code: 500, message: 'API request failed.'});
                }
            });
    });
};

// Filter URLs from nested Object
let filterURLs = (data, value, filteredURLs = []) => {
    Object.keys(data).some(key => {
        let curr = data[key];
        if (typeof curr === 'object' || curr instanceof Array) {
            filteredURLs = filterURLs(curr, value, filteredURLs);
        } else if (curr.includes(API_BASE_PATH)) {
            filteredURLs.push(curr);
        }
    });

    return filteredURLs;
};

// Map API Data with corresponding URLs by replacing URL with the data
let mapAPIData = (data, searchForValue, replaceWithValue = {}) => {
    for (var key in data) {
        let curr = data[key];
        if (typeof curr === 'object' || curr instanceof Array) {
            mapAPIData(curr, searchForValue, replaceWithValue);
        } else if (curr.includes(searchForValue)) {
            data[key] = replaceWithValue;
        }
    }
};

// Remove items from an Array that doesn't match valid list of URIs
let cleanLocationSensitiveContent = (data, validURIs = []) => {
    data['locationSensitiveContent'] = data['locationSensitiveContent'].filter(elem => {
        return validURIs.includes(elem['annotation_uri']);
    });
};

// Create a unique set of URLs to fetch data from
// Apply Promise to the map of URLs (and Fetch API Data)
let processAPIResponse = (data) => {
    return Promise.all(Array
        .from(new Set(filterURLs(data.parms.asyncProps)))
        .map(fetchAPIData)
    )
    .then((results) => {
        const validURIs = data.parms.props.annotation_uri;
        results.forEach(elem => {
            cleanLocationSensitiveContent(elem, validURIs);
            mapAPIData(data, elem.id, elem);
        });

        return data;
    })
    .catch((err) => {
        return {
            code: 500,
            message: 'API request failed.'
        }
    });
}

app.get('/facade/city/:cityName', (req, res) => {
    let cityName = req.params.cityName;

    // First character must be an UPPERCASE letter
    if (!/^[A-Z][a-z]*$/.test(cityName)) {
        return res.status(400).send({error: 'Invalid value for city.'});
    }

    let options = Object.assign({}, defaultOptions);
    options.uri = `${API_URL}/${cityName}`;

    console.log('GET /facade/city', 'Sending request to external API:', options);

    Request.get(options)
        .then(processAPIResponse)
        .then(response => {
            if (response.code && response.code == 500) {
                console.error('GET /facade/city', 'Problem with response:', 'Promise failed.', response.message);
                res.status(response.code).json({error: response.message});
            } else {
                res.json(response);
            }
        })
        .catch((err) => {
            if (err.error && err.error.code && err.error.message) {
                console.error('GET /facade/city', 'Problem with request:', err.error);
                res.status(err.error.code).json({error: err.error.message});
            } else {
                console.error('GET /facade/city', 'Problem with request:', 'API request failed.');
                res.status(500).json({error: 'API request failed.'});
            }
        });
});

// Default Route Handler
app.use((req, res, next) => {
    res.status(404).json({error: 'No route found!'});
});