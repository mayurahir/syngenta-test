# Syngenta Code challange

Consume the API endpoint at https://0-0-5.inyourarea.co.uk/facade/city/:cityName

1. **_cityName_** is capitalised, for instance _London_, _Liverpool_, _Manchester_, _Bath_. Only works for cities, not towns or other settlement types.
2. The endpoint returns a number of news articles mentioning the specified city in the _parms.asyncProps.feed_ property, each having a _mainArticle_ property and optionally a number of _relatedArticles_.

The _mainArticle_ and _relatedArticles_ properties are dehydrated (ie. they are specified as urls to the real (hydrated) data).

### Challenge:
Write a hydration api that, as efficiently as possible, replaces all urls in the _asyncProps_ object with the objects returned by querying those urls.

Make sure this hydration api is agnostic of the structure in _asyncProps_. If the structure changes, it should still parse the entire object in _asyncProps_ and hydrate all urls in it.

The _locationSensitiveContent_ property of each article has redundant info (relating to other locations the same article was matched against). Filter this down to only the relevant ones by matching the _annotation_uri_ prop in each with _parms.props.annotation_uri_ in the overall endpoint data returned in 1. above.

## Prerequisites

- [Node.js](https://nodejs.org/en/download/) - v8.11.3 higher (current LTS)
- NPM - v5.0 or higher

```
node -v
npm -v
```

## Usage (local)

```
npm install
node main.js
```

- Landing page: http://localhost:3000/
- City API: http://localhost:3000/facade/city/:cityName e.g. http://localhost:3000/facade/city/London

## Usage (staging)

https://devcenter.heroku.com/articles/getting-started-with-nodejs#introduction
```
heroku login
heroku create syngenta-test
git push heroku master
heroku open
heroku logs --tail
```

- Landing page: https://syngenta-test.herokuapp.com/
- City API: https://syngenta-test.herokuapp.com/facade/city/:cityName e.g. https://syngenta-test.herokuapp.com/facade/city/London

## License

ISC © Mayur Ahir - see the [LICENSE.md](LICENSE.md) file for details
